"use strict"
let app = function () {
    this.questions =
        [
            "q1 dsf sd  f sd fs df sd fsdsfsf",
            "q2sdfsdf sdf s d f s df ds f",
            "q3sdfsdf sdfsdfds fsfds fds fsd",
            "q4sdfsdfsd sdfsd sd fsdfds ffsdsdf",
            "q5sdfsdf sdf sdfsdfs  sdfsdf s",
            "q6sdf sdffsd dsf dsfsdfsdf dsfsd fsdf",
            "q7",
            "q8",
            "q9",
            "q10",
            "q11",
            "q12",
            "q13",
            "q14",
            "q15",
            "q16",
            "q17",
            "q18",
            "q19",
            "q20",
            "q21",
            "q22",
            "q23",
            "q24",
            "q25"
        ];

}

app.prototype.initState = function () {
    return this.questions.map(q => {
        return {
            isSelected: false,
            question: q
        }
    });
}

app.prototype.onCellClick = function (x, y) {
    console.log("click on : ", x + 5 * y);
    this.state[x + 5 * y].isSelected = !this.state[x + 5 * y].isSelected;
    this.render();
}

app.prototype.renderCell = function (x, y, cellState) {
    let cell = document.createElement("div");
    cell.setAttribute("class", "cell");
    let innerCell = document.createElement("div");
    let idx = x + 5 * y;
    innerCell.setAttribute("id", "c" + idx);
    innerCell.setAttribute("class", cellState.isSelected ? "innercell selected" : "innercell unselected");
    innerCell.innerText = cellState.question;
    innerCell.onclick = this.onCellClick.bind(this, x, y);
    cell.appendChild(innerCell);
    return cell;
}

app.prototype.render = function () {
    let rootDiv = document.createElement("div");
    rootDiv.setAttribute("class", "grid")
    if (!this.state) {
        this.state = this.initState();
    }
    for (let y = 0; y < 5; y++) {
        for (let x = 0; x < 5; x++) {
            let cell = this.renderCell(x, y, this.state[x + 5 * y]);
            rootDiv.appendChild(cell);
        }
    }

    let app = document.getElementById("app");
    let previousBingo = document.getElementById("bingo");
    if (previousBingo) {
        app.removeChild(previousBingo);
    }

    let bingo = document.createElement("div");
    bingo.setAttribute("id", "bingo");
    bingo.appendChild(rootDiv);

    app.appendChild(bingo);
}

let myApp = new app();
myApp.render();